#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export EDITOR=kak

# Custom env
export _LINUX=~/proj/kernel/work/linux
export _LINUX_CONF=~/proj/kernel/work/config
export _LINUX_COV=~/proj/kernel/work/coverage
export _OLDGCC=/usr/local/gcc6.5/bin

###############
### Aliases ###
###############

alias ls='ls -hF --color=auto'
alias l='ls -la'
alias grep='grep --color=auto'
alias df='df -h'
alias ip='ip -c'
alias tree='tree -C'
alias less='less --use-color'
alias info='info --vi-keys'
alias ..='cd ..'
alias cdl='cd $_LINUX'
alias kw-kunit='~/proj/kernel/work/linux/tools/testing/kunit/kunit.py'

#################
### Functions ###
#################

# Upload file to 0x0.st and copy it link to clipboard
zxz(){ printf "$1\n $(curl -F file="@$1" http://0x0.st)" | xsel -b ;}

# Run things detached from the shell session
det() { "$@" &> /dev/null & disown; }

###########
### PS1 ###
###########

# Set terminal title
case ${TERM} in
    [aEkx]term*|rxvt*|gnome*|konsole*|interix|tmux*)
        PS1='\[\033]0;\u@\h:\w\007\]'
        ;;
    screen*)
        PS1='\[\033k\u@\h:\w\033\\\]'
        ;;
    *)
        unset PS1
        ;;
esac

# Error icon if last command exit on error
PS1+='$([[ $? != 0 ]] && echo "\[\e[1;31m\]\342\234\227 ")'

# Different colors for root and regular users
if ((UID)); then
    PS1+='\[\e[01;95m\]\u\[\e[01;97m\]@\[\e[01;95m\]\h\[\e[01;97m\]:\[\e[01;34m\]\w'
else
    PS1+='\[\e[01;91m\]\u\[\e[01;97m\]@\[\e[01;91m\]\h\[\e[01;97m\]:\[\e[01;34m\]\w'
fi

# Git branch
PS1+=' \[\e[01;96m\]$(git branch &>/dev/null && echo -n "("$(git branch 2>/dev/null | sed -n "s/* \(.*\)/\1/p")")")'

# Prompt
PS1+='\n\[\e[01;97m\]\$ \[\e[00m\]'
